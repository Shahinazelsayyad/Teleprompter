package com.example.chahinaz.teleprompter.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;

/**
 * Created by shahinaz-salah on 8/24/17.
 */

public class NewsDB extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1 ;
    public static final String DATABASE_NAME = "Market.db";
    private static final String ENCODING_SETTING = "PRAGMA encoding ='windows-1256'";
    Context context;

    public NewsDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        if (!db.isReadOnly()) {
            db.execSQL(ENCODING_SETTING);
        }
    }

    public void onCreate(SQLiteDatabase db) {
        try{

            final String SQL_CREATE_ENTRIES_FAV =
                    "CREATE TABLE " + NewsTables.News.TABLE_NAME + " (" +
                            NewsTables.News.COLUMN_NAME_ID + " INTEGER PRIMARY KEY, " +
                            NewsTables.News.COLUMN_NAME_ISDELETED + " INTEGER, " +
                            NewsTables.News.COLUMN_NAME_TITLE + " TEXT)";

            db.execSQL(SQL_CREATE_ENTRIES_FAV);

        }catch (Exception ex){
            Log.e("create database error", ex.getMessage());
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        final String SQL_CREATE_ENTRIES_FAV =
                "DROP TABLE IF EXISTS " + NewsTables.News.TABLE_NAME ;
        db.execSQL(SQL_CREATE_ENTRIES_FAV);

        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long insertNews(String title){
        long newRowId = -1;
        try{
            // Gets the data repository in write mode
            SQLiteDatabase db = this.getWritableDatabase();

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(NewsTables.News.COLUMN_NAME_TITLE, title);
            values.put(NewsTables.News.COLUMN_NAME_ISDELETED, 0);

             newRowId = db.insert(NewsTables.News.TABLE_NAME, null, values);

        } catch (Exception ex){
            Log.e("insert news", ex.getMessage());
        }
        return newRowId;
    }

    public ArrayList<String> selectNews(){

        ArrayList<String> news = null;
        try{

            // Gets the data repository in write mode
            SQLiteDatabase db = this.getReadableDatabase();

            String[] projection = {
                    NewsTables.News.COLUMN_NAME_ID,
                    NewsTables.News.COLUMN_NAME_TITLE
            };

            String selection = NewsTables.News.COLUMN_NAME_ISDELETED + " = ?";

            String[] selectionArgs = {"0"};

            // How you want the results sorted in the resulting Cursor
            String sortOrder =
                    NewsTables.News.COLUMN_NAME_ID + " ASC";

            Cursor cursor = db.query(
                    NewsTables.News.TABLE_NAME,                     // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );

            news = new ArrayList<>();
            while(cursor.moveToNext()) {
                String title = cursor.getString(cursor.getColumnIndexOrThrow(NewsTables.News.COLUMN_NAME_TITLE));

                news.add(title);
            }
            cursor.close();
            return news;
        }catch (Exception ex){
            Log.e("select news", ex.getMessage());
        }
       return news;
    }
}


