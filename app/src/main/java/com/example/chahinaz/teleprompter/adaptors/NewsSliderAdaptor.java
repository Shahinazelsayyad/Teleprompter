package com.example.chahinaz.teleprompter.adaptors;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.chahinaz.teleprompter.MainActivity;
import com.example.chahinaz.teleprompter.R;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by shahinaz-salah on 11/26/17.
 */

public class NewsSliderAdaptor extends PagerAdapter {

        private Activity activity;
        private ArrayList<String> newsArrayList;
        int defaultSize = 12;
        int itemPosition;
        View viewItem;
        ArrayList<TextView> views;
        ViewGroup containerNews;
        TextView newsTxt;

        public NewsSliderAdaptor(Activity activity, ArrayList<String> newsArrayList){
            this.activity = activity;
            this.newsArrayList = newsArrayList;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            LayoutInflater inflater = ((Activity)activity).getLayoutInflater();

            viewItem = inflater.inflate(R.layout.rotating_text_view, container, false);
             newsTxt = (TextView) viewItem.findViewById(R.id.rotating_news_txt);
            LinearLayout animatedContainer = (LinearLayout) viewItem.findViewById(R.id.container);
            try{
                newsTxt.setText(newsArrayList.get(position));
                views.add(newsTxt);
                itemPosition = position;
                containerNews = container;
            }catch (Exception ex){
                ex.printStackTrace();
            }
            ((ViewPager)container).addView(viewItem);
            return viewItem;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return newsArrayList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            // TODO Auto-generated method stub
            return view == ((View)object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // TODO Auto-generated method stub
            ((ViewPager) container).removeView((View) object);
        }

    public void changeTextSize(int size){
        try{
            defaultSize = size;
            newsTxt.setTextSize(size);
            for(int i = 0 ; i < views.size(); i++){
                views.get(i).setTextSize(size);
            }
            for(int i = 0; i < containerNews.getChildCount(); i++){
                TextView txt = (TextView) containerNews.getChildAt(i);
                txt.setTextSize(size);
            }
        }catch (Exception ex){
            ex.getMessage();
        }
    }
}
