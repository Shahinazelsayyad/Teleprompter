package com.example.chahinaz.teleprompter.database;

import android.provider.BaseColumns;

public final class NewsTables {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private NewsTables() {}

    /* Inner class that defines the table contents */
    public static class News implements BaseColumns {
        public static final String TABLE_NAME = "news";
        public static final String COLUMN_NAME_ID = "newsID";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_ISDELETED = "iddeleted";
    }

}
