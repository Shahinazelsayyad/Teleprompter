package com.example.chahinaz.teleprompter.backgroundtasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.example.chahinaz.teleprompter.handlers.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Chahinaz on 29/12/2017.
 */

public class GetBBCNewsService extends AsyncTask<String , Void , String>
implements ServicesCallBackResult{

    private Context context;
    private ArrayList<String> newsTitle;
    ServicesCallBackResult delegate;

    public GetBBCNewsService(Context context, ServicesCallBackResult delegate) {
        this.context = context;
        this.delegate = delegate;
    }

    @Override
    protected String doInBackground(String... params) {

        String link = Constant.APILinks.BBCNewsAPI;
        String Result="";

        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();

            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
            StringBuffer buffer = new StringBuffer();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }
            Result = buffer.toString();

        } catch (java.io.IOException e) {
            e.printStackTrace();
        }catch (Exception ex){
            ex.getMessage();
        }

        getNews(Result);
        return Result;
    }

    public String getNews(String result)
    {
        newsTitle = new ArrayList<>();
        try{
            JSONObject jsonNews = new JSONObject(result);
            JSONArray arr = jsonNews.getJSONArray("articles");
            for (int i = 0 ; i < arr.length() ; i++)
            {
                String title = arr.getJSONObject(i).getString("title");
                newsTitle.add(title);
            }

        }catch (JSONException ex) {
            Log.e("Error", ex.getMessage());
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        delegate.OnResponseReceived(newsTitle);
    }

    @Override
    public void OnResponseReceived(Object result) {
    }
}
