package com.example.chahinaz.teleprompter;

import android.app.Activity;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chahinaz.teleprompter.adaptors.NewsSliderAdaptor;
import com.example.chahinaz.teleprompter.backgroundtasks.GetBBCNewsService;
import com.example.chahinaz.teleprompter.backgroundtasks.ServicesCallBackResult;
import com.example.chahinaz.teleprompter.database.NewsDB;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
implements SeekBar.OnSeekBarChangeListener{

    @BindView(R.id.news_sliderPager)
    ViewPager newsSliderPager;

    @BindView(R.id.news_edit_text)
    EditText newNewsEditTxt;

    @BindView(R.id.setting_container)
    LinearLayout settingContainer;

    @BindView(R.id.sizeSeekbar)
    SeekBar textSizeSeek;

    @BindView(R.id.speedSeekbar)
    SeekBar speedSeek;

    @BindView(R.id.text_volume)
    TextView fontSizeTxt;

    @BindView(R.id.text_speed)
    TextView speedTxt;

    @BindView(R.id.bcc_news_btn)
    Button bbcNewBtn;

    @BindView(R.id.progressbar)
    ProgressBar progressbar;

    Animation left_to_right_animation;
    Timer timer;
    final long DELAY_MS = 0;
    long PERIOD_MS = 5;
    ArrayList<String> newsArray;
    int currentPage = 0;
    NewsSliderAdaptor adp;
    NewsDB db;
    int loops;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        db = new NewsDB(MainActivity.this);
        newsArray = new ArrayList<>();
        //get saved news by user
        newsArray = db.selectNews();

        if(newsArray.size() == 0){
            //set first news item for test
            newsArray.add("My first new");
        }
        hideProgressBar();
        setFragmentTitle(getResources().getString(R.string.main_title));
        //the font size and speed seek listners
        textSizeSeek.setOnSeekBarChangeListener(this);
        speedSeek.setOnSeekBarChangeListener(this);

        //fill the news data and start the animation
        fillNewsData();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        try{
            switch (seekBar.getId()){
                case R.id.sizeSeekbar: // the size seeker
                    fontSizeTxt.setText(String.valueOf(i));
                    adp.changeTextSize(i);
                    break;

                case R.id.speedSeekbar:  //the speed seeker
                    speedTxt.setText(String.valueOf(i));
                    PERIOD_MS = i;
                    break;
            }
        }catch (Exception ex){
            ex.getMessage();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.setting_menu, menu);//Menu Resource, Menu
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.setting:
                if(settingContainer.getVisibility() == View.VISIBLE){
                    settingContainer.setVisibility(View.INVISIBLE);
                }else {
                    settingContainer.setVisibility(View.VISIBLE);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.new_news_btn)
    public void onAddNewButtonClickHandler(){
        if(!newNewsEditTxt.getText().toString().equals("")){
            db.insertNews(newNewsEditTxt.getText().toString());
            newsArray = db.selectNews();
            newNewsEditTxt.setText("");
            hideSoftKeyboard();
            fillNewsData();
        }
    }

    @OnClick(R.id.bcc_news_btn)
    public void newsButtonHandler(){
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask backtask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            Toast.makeText(MainActivity.this, "running", Toast.LENGTH_SHORT).show();
                            getBBCNewsService();
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }
                });
            }
        };
        timer.schedule(backtask , 0, 600000); //execute in every 20000 ms*/
    }

    protected void getBBCNewsService(){
                GetBBCNewsService service = new GetBBCNewsService(MainActivity.this, new ServicesCallBackResult() {
                    @Override
                    public void OnResponseReceived(Object result) {
                        hideProgressBar();
                        newsArray = (ArrayList<String>) result;
                        if(!(newsArray.size() == 0) && !(newsArray == null)){
                            adp.notifyDataSetChanged();
                            fillNewsData();
                        }
                    }
                });
                service.execute();
                showProgressBar();
    }

    private void fillNewsData() {
        try{
            adp = new NewsSliderAdaptor(MainActivity.this, newsArray);
            newsSliderPager.setAdapter(adp);
            startThread();
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    protected void startThread(){

        try{
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {

                    if (currentPage == newsArray.size()) {
                        currentPage = 0;
                    }
                    newsSliderPager.setCurrentItem(currentPage++, true);
                    startAnimation(newsSliderPager);
                }
            };

            timer = new Timer(); // This will create a new Thread
            timer .schedule(new TimerTask() { // task to be scheduled

                @Override
                public void run() {
                    handler.post(Update);
                }
            }, DELAY_MS, PERIOD_MS * 1000);

        }catch (Exception ex){
            ex.getMessage();
        }
    }

    protected void startAnimation(final View container){

        try{
            loops = 1000000;
            left_to_right_animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.left_to_right);
            container.animate().setDuration(loops * 1000).setInterpolator(new LinearInterpolator());
            container.startAnimation(left_to_right_animation);
            left_to_right_animation.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }
            });
        }catch (Exception ex){
            ex.getMessage();
        }

    }

    public void hideSoftKeyboard() {
        Activity activity = MainActivity.this;
        if (activity != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null && activity.getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public void showProgressBar() {
        progressbar.setVisibility(View.VISIBLE);
        progressbar.bringToFront();
    }

    public void hideProgressBar(){
        progressbar.setVisibility(View.GONE);
    }

    public void setFragmentTitle(String title) {
        getSupportActionBar().setTitle(title);
    }


    public class StartServiceCountDownTimer extends CountDownTimer {

        public StartServiceCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            getBBCNewsService();
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }
}
