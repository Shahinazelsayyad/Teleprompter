package com.example.chahinaz.teleprompter.backgroundtasks;

/**
 * Created by Chahinaz on 07/07/2017.
 */

public interface ServicesCallBackResult {

    public  void OnResponseReceived(Object result);

}
